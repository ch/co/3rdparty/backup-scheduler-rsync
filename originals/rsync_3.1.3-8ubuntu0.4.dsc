-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: rsync
Binary: rsync
Architecture: any
Version: 3.1.3-8ubuntu0.4
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Uploaders: Samuel Henrique <samueloph@debian.org>
Homepage: https://rsync.samba.org/
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/debian/rsync
Vcs-Git: https://salsa.debian.org/debian/rsync.git
Testsuite: autopkgtest
Testsuite-Triggers: @builddeps@
Build-Depends: debhelper-compat (= 12), libacl1-dev, libattr1-dev, libpopt-dev, yodl (>= 3.08.00)
Package-List:
 rsync deb net optional arch=any
Checksums-Sha1:
 82e7829c0b3cefbd33c233005341e2073c425629 905908 rsync_3.1.3.orig.tar.gz
 bdbc582dd294f625f905ca16edb1697fbc89809a 37624 rsync_3.1.3-8ubuntu0.4.debian.tar.xz
Checksums-Sha256:
 55cc554efec5fdaad70de921cd5a5eeb6c29a95524c715f3bbf849235b0800c0 905908 rsync_3.1.3.orig.tar.gz
 d482dfee1b6640f0b2967881d8e0bb011e7f4d556027de8af938c0b1a391b3ff 37624 rsync_3.1.3-8ubuntu0.4.debian.tar.xz
Files:
 1581a588fde9d89f6bc6201e8129afaf 905908 rsync_3.1.3.orig.tar.gz
 6d7f6084384edc173e9408d030a5747f 37624 rsync_3.1.3-8ubuntu0.4.debian.tar.xz
Original-Maintainer: Paul Slootman <paul@debian.org>

-----BEGIN PGP SIGNATURE-----

iQJNBAEBCgA3FiEELTsQ/oZuJMqL99Qt1guDyQUTvU8FAmL+oVUZHG1hcmsuZXNs
ZXJAY2Fub25pY2FsLmNvbQAKCRDWC4PJBRO9TzBJD/4ueNprLsYNMp3YPm8EAXfE
Gyv33HUkQ/zjylA0j/v5F/CuVo0ENM7bqUl7GmzXils+oLo4Z2NrcmXdbB/TriRZ
GS8e+MaO3AbqMyKgJggCWPs7BPCXBgKBSlhiluEV1iCmNVUP400fvUPrNbQYGzrq
eLp5HjnX3C+dwEcPdhMrr2G0F/l13qVCqeF/MAMfcO3LsNHduusUWHb1Ns1KBc/3
ofTILPLpS4BIqKNnwxGKwlR4vt51/a9RuWNlBn8NeBH9K5WF3xSD9GN4Zm0bqieD
Ub/rmBHSlrku+od82JriRxzU/rwE8e422XWZagUjicZk+72HIwKCQ4gH/ZiCeC4u
TQFQOyrCDg+TsX5yFNvdmnyeGuuN7TBKTV6Bku7DXOanS1fR/97+PrFu7b4SshqZ
bre8dB5yYV2QGlIaVQS1i3EOM+WkfyeGaVRE5oXB2SAIdVCjh3jjfNYx2PsFis4x
F/baBv3ufcTeHBO7Z9MomYpFj1R8PhPSPnX2HbsASWofoH2Q4RPY0zVcUbkuFiNB
kGHYJgymVkVtJfW7zJ/7DqEBDzquVSxGm7n37x4macLGhytf/K7w83Zlvu51bMwH
T8yJldG9soZhbR2Xcq8SYW32xmEQfNFRGGoZWzvHkpbFzdlQotA6WYUDxmUwwQcO
LnY0T7mGHFwjgguK4ydFfw==
=kdoz
-----END PGP SIGNATURE-----
