Format: 3.0 (quilt)
Source: backup-scheduler-rsync
Binary: backup-scheduler-rsync
Architecture: any
Version: 3.1.3-ch2
Maintainer: Chemistry COs <support@ch.cam.ac.uk>
Uploaders: Samuel Henrique <samueloph@debian.org>
Homepage: https://rsync.samba.org/
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/debian/rsync
Vcs-Git: https://salsa.debian.org/debian/rsync.git
Testsuite: autopkgtest
Testsuite-Triggers: @builddeps@
Build-Depends: debhelper-compat (= 12), libacl1-dev, libattr1-dev, libpopt-dev, yodl (>= 3.08.00)
Package-List:
 backup-scheduler-rsync deb net optional arch=any
Checksums-Sha1:
 82e7829c0b3cefbd33c233005341e2073c425629 905908 backup-scheduler-rsync_3.1.3.orig.tar.gz
 a08ac2025f6e7007baf612a68f5c1b75a31117c8 39240 backup-scheduler-rsync_3.1.3-ch2.debian.tar.xz
Checksums-Sha256:
 55cc554efec5fdaad70de921cd5a5eeb6c29a95524c715f3bbf849235b0800c0 905908 backup-scheduler-rsync_3.1.3.orig.tar.gz
 f93fa25c06662070aabec385a335b9d484752ca4a4b6b80445c1c3b2dac0d336 39240 backup-scheduler-rsync_3.1.3-ch2.debian.tar.xz
Files:
 1581a588fde9d89f6bc6201e8129afaf 905908 backup-scheduler-rsync_3.1.3.orig.tar.gz
 c990042dfb89bf38fca9a56b170a0822 39240 backup-scheduler-rsync_3.1.3-ch2.debian.tar.xz
Original-Maintainer: Paul Slootman <paul@debian.org>
